# wit-router
A router for wit.ai entities. Ranks a set of entities returned from wit.ai based on criteria that you register. Ranking is determined by averaging the 2 ranks for each comparison - how the
route request entity set matched against each registered route, and how the registered route matched
against the query. This favors matching more specific entity sets in the query rather than simply satisfying 100% of the match criteria of a route, so that a more specific route can be chosen
instead of a simpler/less complete route.

## Usage
```js
const WitRouter = require('wit-router')
const witRouter = new WitRouter()
witRouter.register({
  name: 'version',
  entities: {
    intent: [{
      match: /^version$/
    }]
  },
  callback: function () {
    console.log('hit version route')
  }
})

witRouter.register({
  name: 'version',
  entities: {
    intent: [{
      match: /^version$/
    }],
    server: [{
      match: /^CVP$/
    }]
  },
  callback: function () {
    console.log('hit CVP version route')
  }
})

witRouter.register({
  name: 'version',
  entities: {
    intent: [{
      match: /^version$/
    }],
    server: [{
      match: /^CVP$/
    }],
    service: [{
      match: /^Call Server$/,
      required: true
    }]
  },
  callback: function () {
    console.log('hit CVP Call Server version route')
  }
})

// route to 'CVP Call Server Version'
witRouter.route({
  service: [{
    confidence: 0.90813538787052,
    value: 'Call Server',
    type: 'value'
  }],
  server: [{
    confidence: 0.93483557193052,
    value: 'CVP',
    type: 'value'
  }],
  intent: [{
    confidence: 0.99982729131181,
    value: 'version'
  }]
}).callback()

// route to 'CVP Version'
witRouter.route({
  server: [{
    confidence: 0.93483557193052,
    value: 'CVP',
    type: 'value'
  }],
  intent: [{
    confidence: 0.99982729131181,
    value: 'version'
  }]
}).callback()

// route to 'version'
witRouter.route({
  intent: [{
    confidence: 0.9884404035283,
    value: 'version'
  }]
}).callback()
```
