// get the index of the max value in an array
function indexOfMax(arr) {
  if (arr.length === 0) {
    return -1
  }

  let max = arr[0]
  let maxIndex = 0

  for (const i in arr) {
    if (arr[i] > max) {
      maxIndex = i
      max = arr[i]
    }
  }

  return maxIndex
}

class WitRouter {
  constructor (minConfidence) {
    this.routes = []
    // set minimum accepted confidence to input if valid value was specified
    if (minConfidence && minConfidence >= 0 && minConfidence <= 1.0) {
      this.minConfidence = minConfidence
    } else {
      // or default to 0.75
      this.minConfidence = 0.75
    }
  }

  // register new route
  register (route) {
    // You can only register routes with an 'entities' property
    if (route.entities) {
      const name = route.name || ''
      console.log(`registering route '${name}'`)
      this.routes.push(route)
    } else {
      console.log(`route didn't contain any entities. not adding to routes.`, route)
    }
  }

  // route entity set to the most appropriate endpoint
  route (entities) {
    // get rankings
    const ranks = this.getRanks(entities)
    console.log('route match ranks', ranks)

    // get the index of the highest rank
    const highRankIndex = indexOfMax(ranks)
    // get the highest rank value
    const highRank = ranks[highRankIndex]
    // check that our highest ranks was not 0
    if (highRank > 0) {
      // checks finished - return route with highest score
      return this.routes[highRankIndex]
    } else {
      throw 'no routes matched'
    }
  }

  getRanks(entities) {
    // console.log('Object.keys(entities)', Object.keys(entities))
    const entityTypeCount1 = Object.keys(entities).length
    // score each route, then return the highest score
    const ranks1 = []
    const ranks2 = []
    // iterate over registered routes
    loop1:
    for (const i in this.routes) {
      // this score - start at 0
      ranks1[i] = 0
      ranks2[i] = 0
      const route = this.routes[i]
      // get keys array of this route's entities
      const keys = Object.keys(route.entities)
      // console.log('Object.keys(route.entities)', Object.keys(route.entities))
      // count up entity types, for helping calculate the overall rank
      const entityTypeCount2 = keys.length
      // iterate over each entity of this route
      loop2:
      for (const key of keys) {
        // try to match each entity with entities param
        const entityArray = route.entities[key]
        loop3: // iterate over each entity of registered route of type {key}
        for (const entityMatch of entityArray) {
          // if there is a matching entity in input param
          if (this.testEntity(key, entityMatch, entities)) {
            // match
            // increase rank for this route
            // TODO prevent double-matching within this same entityArray
            // console.log(`ranks1[${i}] += 1.0/${entityTypeCount1}/${entities[key].length}`)
            // console.log(`ranks2[${i}] += 1.0/${entityTypeCount2}/${entityArray.length}`)
            ranks1[i] += 1.0/entityTypeCount1/entities[key].length
            ranks2[i] += 1.0/entityTypeCount2/entityArray.length
          } else {
            // no match
            // check if this is required
            if (entityMatch.required) {
              console.log('required entity')
              // required
              // set score to 0
              ranks1[i] = 0
              ranks2[i] = 0
              // this is not a matching route - continue in next iteration loop1
              continue loop1
            } else {
              // console.log('no match. continue.')
              // not required - do nothing, just continue
              // continue loop1
            }
          }
        }
      }
    }
    // console.log(ranks1)
    // console.log(ranks2)
    // return average of ranks
    const ranks = []
    for (const i in ranks1) {
      ranks[i] = (ranks1[i] + ranks2[i]) / 2
    }
    return ranks
  }

  // test if a single <key> entity entry (needle) is found in haystack
  testEntity(key, needle, haystack) {
    // console.log('testEntity', key, needle, haystack)
    for (const k of Object.keys(haystack)) {
      // are these of the same entity category/type?
      if (key === k) {
        // iterate over the list of entities of this type in the haystack
        for (const entry of haystack[k]) {
          try {
            // construct regexp
            // const regex = new RegExp(needle.match, 'i')
            const regex = needle.match
            // test if regex matches, and if so, is the confidence high enough?
            if (regex.test(entry.value)) {
              // regex passed - now check for confidence, if exists
              if (needle.confidence) {
                // needle has confidence, so entry must match or exceed it
                return entry.confidence >= needle.confidence
              } else {
                // needle has no confidence specified, so check against default confidence level
                return entry.confidence >= this.minConfidence
              }
            }
          } catch (err) {
            // regexp creation error
            console.log('Error in regular expression, ' + needle.match + ' : ' + err)
            // continue to next entry
            continue
          }
        }
      } else {
        // not of the same type - continue to next iteration
        continue
      }
    }
    // no match found
    // console.log('no match...')
    return false
  }
}

module.exports = WitRouter
